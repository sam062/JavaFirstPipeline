package base;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@GetMapping("/test")
	public String testApi() {
		return "Up and running";
	}
	
	@GetMapping("/wish")
	public String wishApi() {
		return "Hi, Good Morning";
	}

}
